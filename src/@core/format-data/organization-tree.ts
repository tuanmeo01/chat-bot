function constructTreeData(data) {
  return data.map((item) => {
    let o = {
      name: item.organizationName,
      value: item.organizationId,
      children: item.children.length
        ? constructTreeData(item.children)
        : [],
    };
    return o;
  });
}

function checkLeftOvers(leftOvers, possibleParent) {
  for (let i = 0; i < leftOvers.length; i++) {
    if (leftOvers[i].organizationId === possibleParent.organizationParentId) {
      delete leftOvers[i].organizationParentId;
      possibleParent.children
        ? possibleParent.children.push(leftOvers[i])
        : (possibleParent.children = [leftOvers[i]]);
      possibleParent.count = possibleParent.children.length;
      const addedObj = leftOvers.splice(i, 1);
      checkLeftOvers(leftOvers, addedObj[0]);
    }
  }
}

function findParent(possibleParents, possibleChild) {
  let found = false;
  for (let i = 0; i < possibleParents.length; i++) {
    if (
      possibleParents[i].organizationId === possibleChild.organizationParentId
    ) {
      found = true;
      delete possibleChild.organizationParentId;
      if (possibleParents[i].children)
        possibleParents[i].children.push(possibleChild);
      else possibleParents[i].children = [possibleChild];
      possibleParents[i].count = possibleParents[i].children.length;
      return true;
    } else if (possibleParents[i].children)
      found = findParent(possibleParents[i].children, possibleChild);
  }
  return found;
}

export const TreeData = function(data: any[]) {
  return constructTreeData(
    data.reduce(
      (initial, value: any, index, original) => {
        if (
          value.organizationParentId === value.organizationId ||
          value.organizationParentId === null
        ) {
          if (initial.left.length) checkLeftOvers(initial.left, value);
          delete value.organizationParentId;
          initial.nested.push(value);
        } else {
          let parentFound = findParent(initial.nested, value);
          if (parentFound) checkLeftOvers(initial.left, value);
          else initial.left.push(value);
        }
        return index < original.length - 1 ? initial : initial.nested;
      },
      { nested: [], left: [] }
    )
  );
};
