import { CoreMenu } from "@core/types";

export const menu: CoreMenu[] = [
  {
    id: "home",
    title: "Chat",
    translate: "Chat",
    type: "item",
    icon: "message-circle",
    url: "chat",
  },
  {
    id: "sample",
    title: "Information",
    translate: "Information",
    type: "item",
    icon: "info",
    url: "sample",
  },
  {
    id: "sample",
    title: "Config",
    translate: "Config",
    type: "item",
    icon: "settings",
    url: "config",
  },
];
