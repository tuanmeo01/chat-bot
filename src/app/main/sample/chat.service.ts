import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class ChatService {
  constructor(private _http: HttpClient) {}
  Chat(message) {
    const body = {
      question: message,
    };
    return this._http.post<any>(`${environment.apiUrl}/food/answer`, body);
  }
  Rating(body) {
    //     {
    //   "username": "Tuan",
    //   "recipe_name": "tuan",
    //   "rating_value": 2,
    //   "rating_timestamp": "string"
    // }
    return this._http.post<any>(`${environment.apiUrl}/food/rating`, body);
  }
}
