import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { AuthenticationService } from "app/auth/service";
import { ChatService } from "./chat.service";
import { FormControl, Validators } from "@angular/forms";
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  public isChatBot: boolean = false;
  stompClient: any;
  newMessage = "";
  messages: any[];
  socket: any;
  session_id: string;
  userAvatar: string;
  userName: string;
  public contentHeader: any;
  public point: number = 0;
  data = {
    answers: ["2500"],
    data: {
      question: "what calo in 1kg meet",
      question_type: "simple",
      topic_entities: "1kg meet",
      answer: "2500",
    },
    answer_ids: [],
    relation_paths: [],
    error_code: 0,
    error_msg: "Success.",
  };
  get code() {
    return JSON.stringify(this.data, null, 2);
  }

  set code(v) {
    try {
      this.data = JSON.parse(v);
    } catch (e) {
      console.log("error occored while you were typing the JSON");
    }
  }
  public ctrl = new FormControl(null, Validators.required);
  constructor(private _chatService: ChatService) {}

  ngOnInit(): void {
    this.userName = JSON.parse(
      localStorage.getItem("currentUsers")
    ).email.split("@")[0];
    this.contentHeader = {
      headerTitle: "Chat message",
      actionButton: true,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Chat message",
            isLink: true,
            link: "/",
          },
          {
            name: "Chat",
            isLink: false,
          },
        ],
      },
    };
    this.messages = [
      {
        text: `Hello ${this.userName}, How can i help you?`,
        isCustomer: false,
        // data: {
        //   answers: ["2500"],
        //   data: {
        //     question: "what calo in 1kg meet",
        //     question_type: "simple",
        //     topic_entities: "1kg meet",
        //     answer: "2500",
        //   },
        //   answer_ids: [],
        //   relation_paths: [],
        //   error_code: 0,
        //   error_msg: "Success.",
        // },
      },
    ];
  }
  rating(point) {
    console.log(point);
  }
  sendMessage() {
    if (this.newMessage == "") return;
    else {
      // // Gửi tin nhắn của người dùng

      // Thêm tin nhắn của người dùng vào danh sách
      this.messages.push({
        text: this.newMessage,
        isCustomer: true,
      });
      // thêm tin nhắn response của chatbot
      this._chatService.Chat(this.newMessage).subscribe((res) => {
        this.messages.push({
          text: res.data.data.answer,
          isCustomer: false,
          data: res.data,
        });
      });

      this.newMessage = "";
    }
  }
  openChatbot() {
    this.isChatBot = !this.isChatBot;
  }
  formatMessage(message) {
    if (message) {
      var urlRegex = /(https?:\/\/[^\s]+)/g;
      return message?.replace(urlRegex, function (url) {
        return '<a href="' + url + '" target="_blank">' + url + "</a>";
      });
    }
  }
}
