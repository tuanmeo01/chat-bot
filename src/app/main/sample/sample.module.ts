import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { CoreCommonModule } from "@core/common.module";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";

import { SampleComponent } from "./sample.component";
import { HomeComponent } from "./home.component";
import { NgbAccordionModule, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AuthGuard } from "app/auth/helpers";
import { NgxJsonViewerModule } from "ngx-json-viewer";
import { PdfViewerModule } from "@syncfusion/ej2-angular-pdfviewer";

const routes = [
  {
    path: "sample",
    component: SampleComponent,
    data: { animation: "sample" },
  },
  {
    path: "config",
    component: SampleComponent,
    data: { animation: "sample" },
  },
  {
    path: "chat",
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [SampleComponent, HomeComponent],
  imports: [
    RouterModule.forChild(routes),
    ContentHeaderModule,
    TranslateModule,
    CoreCommonModule,
    NgbModule,
    NgbAccordionModule,
    NgxJsonViewerModule,
    PdfViewerModule,
  ],
  exports: [SampleComponent, HomeComponent],
})
export class SampleModule {}
