FROM node:14.17.5
WORKDIR /usr/src/app
RUN npm install -g @angular/cli@12.0.3
COPY . /usr/src/app
RUN npm install
RUN export NODE_OPTIONS=--max_old_space_size=8192
RUN ng build --output-path=./dist/food-qa --output-hashing=all --source-map=false --vendor-chunk=false --aot --build-optimizer
RUN mv dist/food-qa/index.html dist/index.html
EXPOSE 4200
CMD ["npx", "angular-http-server", "--path", "dist/", "-p", "4200", "--open"]